/// <reference types="cypress" />

import { MenuPO } from '../page_objects/menuPO'
import { GetQuotePO } from '../page_objects/getQuotePO'

const menuPO = new MenuPO()
const getQuotePO = new GetQuotePO()

describe('Test Get Quote', function() {
    before(function() {
        cy.fixture('quote').then(function(data) 
        {
            this.data = data
        })
    })
    it('Test mandatory fields', function() {
        cy.visit(Cypress.env('url'))
        menuPO.menuGetQuote().click()
        getQuotePO.btnContinue().click()
        getQuotePO.btnSubmit().click()
        getQuotePO.tfInput(0, 'input').should('have.class', 'input is-danger')
        getQuotePO.tfInput(0, 'span').as('parent').should('exist')
        cy.get('@parent').find('i').should('exist')
        
        getQuotePO.tfInput(0, 'input').type(this.data.treatment)
        getQuotePO.btnContinue().click()
        getQuotePO.btnSubmit().click()
        getQuotePO.tfInput(0, 'input').should('have.class', 'input')
        getQuotePO.tfInput(0, 'span').should('not.exist')
        getQuotePO.tfInput(2, 'input').should('have.class', 'input is-danger')
        getQuotePO.tfInput(2, 'span').as('parent').should('exist')
        cy.get('@parent').find('i').should('exist')

        getQuotePO.tfInput(2, 'input').type(this.data.name)
        getQuotePO.btnSubmit().click()
        getQuotePO.tfInput(2, 'input').should('have.class', 'input')
        getQuotePO.tfInput(2, 'span').should('not.exist')
        getQuotePO.tfInput(3, 'input').should('have.class', 'input is-danger')
        getQuotePO.tfInput(3, 'span').as('parent').should('exist')
        cy.get('@parent').find('i').should('exist')

        getQuotePO.tfInput(3, 'input').type(this.data.email)
        getQuotePO.btnSubmit().click()
        getQuotePO.tfInput(3, 'input').should('have.class', 'input')
        getQuotePO.tfInput(3, 'span').should('not.exist')
        getQuotePO.tfInput(4, 'input').should('have.class', 'input is-danger')
        getQuotePO.tfInput(4, 'span').as('parent').should('exist')
        cy.get('@parent').find('i').should('exist')

        getQuotePO.tfInput(4, 'input').type(this.data.phone)
        getQuotePO.btnSubmit().click()
        cy.url().should('eq', 'https://www.doctorxdentist.com/quote-request/new')
    })
})
/// <reference types="Cypress" />

import { MenuPO } from '../page_objects/menuPO'
import { FindDocPO } from '../page_objects/findDocPO'
import { FindDocPF } from '../page_functions/findDocPF'

const menuPO = new MenuPO()
const findDocPO = new FindDocPO()
const findDocPF = new FindDocPF()

describe('Test Find a Doctor Filter', () => {
    it('Test filter by search', () => {
        cy.visit(Cypress.env('url'))
        cy.fixture('search').then(d => {
            menuPO.menuFindDoc().click()
            findDocPO.tfSearch().type(d.field).type('{enter}')
            findDocPO.lblDoc().each(el => {
                let res = false;
                const arrStr = el.text().split(' ')
                for (const str of arrStr) {
                    if (d.field.includes(str)) {
                        res = true;
                        break;
                    } 
                }
                expect(res).to.be.true
            });
        })
    })
    it('Test filter by regions', () => {
        cy.visit(Cypress.env('url'))
        cy.fixture('search').then(d => { 
            menuPO.menuFindDoc().click()
            findDocPF.SelectRegions(d.regions)
        })
    })
    it('Test filter by insurances', () => {
        cy.visit(Cypress.env('url'))
        cy.fixture('search').then(d => { 
            menuPO.menuFindDoc().click()
            findDocPF.SelectInsurances(d.insurances)
        })
    })
})
/// <reference types="Cypress" />
import { FindDocPO } from "../page_objects/findDocPO";
const findDocPO = new FindDocPO()
export class FindDocPF {
    SelectRegions(data) {
        for (const d of data) {
            // findDocPO.ddlRegions().click()
            findDocPO.lblRegions().each((el, idx) => {
                if (d === el.text().trim()) {
                    findDocPO.ckbRegions().eq(idx).check({ force: true})
                    return false
                }
            })
        }
        findDocPO.lblRegionCount().should('contain.text', data.length)
    }
    SelectInsurances(data) {
        for (const d of data) {
            // findDocPO.ddlInsurances().click()
            findDocPO.lblInsurances().each((el, idx) => {
                if (d === el.text().trim()) {
                    findDocPO.ckbInsurances().eq(idx).check({ force: true })
                    return false
                }
            })
        }
        findDocPO.lblInsuranceCount().should('contain.text', data.length)
    }
}
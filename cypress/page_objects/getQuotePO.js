/// <reference types="Cypress" />

export class GetQuotePO {
    tfInput = (idx, el) => cy.get('.is-clearfix').eq(idx).find(el)
    btnContinue = () => cy.get('.tab-item > .button.is-primary')
    btnBack = () => cy.get("[class='button is-large']")
    btnSubmit = () => cy.get(".is-half [class*='is-primary is-large']")
}
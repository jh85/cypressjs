/// <reference types="Cypress" />

export class MenuPO {
    menuFindDoc = () => cy.get('.navbar-item > a:last-child')
    menuGetQuote = () => cy.get('.navbar-item > a:nth-child(3)')
}
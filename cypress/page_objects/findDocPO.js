/// <reference types="Cypress" />

export class FindDocPO {
    tfSearch = () => cy.get('.input')
    ddlRegions = () => cy.get('.field.column:nth-child(2) .trigger')
    ckbRegions = () => cy.get('.is-gapless > :nth-child(2) input')
    lblRegions = () => cy.get('.is-gapless > :nth-child(2) .control-label')
    lblRegionCount = () => cy.get('.is-gapless > :nth-child(2) .trigger > span')
    ddlInsurances = () => cy.get('.field.column:nth-child(3) .trigger')
    ckbInsurances = () => cy.get('.is-gapless > :nth-child(3) input')
    lblInsurances = () => cy.get('.is-gapless > :nth-child(3) .control-label')
    lblInsuranceCount = () => cy.get('.is-gapless > :nth-child(3) .trigger > span')
    btnSubmit = () => cy.get("[type='submit']")
    ckbFreeVid = () => cy.get('.has-border .check')
    lblDoc = () => cy.get('.name.title')
}